# Running project
`composer install`

then

`docker-compose up -d `

or

`php -S localhost:8080 -t public` 

# Tasks

```
1.
    - Check http://localhost:8080/slow/worker
    - Move call of doSomeJob() to kernel.terminate event

2. 
    - Check http://localhost:8080/with/exception
    - Make cusom handler just for VeryCustomException on kernel.exception event

3.
    - Check http://localhost:8080/with/html
    - Add some GA tracking code to to each html response on kernel.response event

4.
    - Check http://localhost:8080/json/response It's error now
    - Add event listener on kernel.view event and convert it json

5.
    - Look at StrategyController
    - Add one more strategy for salary calculation, for example % of product income, for product data providing income use separated service  
```

# Useful links

- https://symfony.com/doc/current/introduction/http_fundamentals.html#requests-and-responses-in-php
- https://symfony.com/doc/current/components/http_kernel.html
- https://symfony.com/doc/current/service_container/autowiring.html
- https://symfony.com/blog/new-in-symfony-3-3-service-autoconfiguration
- https://symfony.com/doc/current/service_container.html
- https://symfony.com/doc/current/controller/error_pages.html#working-with-the-kernel-exception-event

# P.S.
- To see all routes use `bin/console debug:router`
- To see event subscribers use `bin/console debug:event-dispatcher` or `bin/console debug:event-dispatcher
*event.name*`
