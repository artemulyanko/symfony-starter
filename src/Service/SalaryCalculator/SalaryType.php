<?php


namespace App\Service\SalaryCalculator;


final class SalaryType
{
    public const HOURLY = 'hourly';
    public const MONTHLY = 'monthly';
}
