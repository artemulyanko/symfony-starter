<?php


namespace App\Service\SalaryCalculator;


use App\Model\Employee;

class FixedSalaryStrategy implements SalaryCalculatorStrategyInterface
{
    public function supports(string $salaryType): bool
    {
        return $salaryType === SalaryType::MONTHLY;
    }

    public function calculateSalary(Employee $employee, \DateTimeInterface $date): int
    {
        return $employee->getFixedSalary();
    }
}
