<?php


namespace App\Service\SalaryCalculator;


use App\Model\Employee;

interface SalaryCalculatorStrategyInterface
{
    public function supports(string $salaryType): bool;
    public function calculateSalary(Employee $employee, \DateTimeInterface $date): int;
}
