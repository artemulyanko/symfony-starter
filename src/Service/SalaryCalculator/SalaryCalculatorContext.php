<?php


namespace App\Service\SalaryCalculator;


use App\Model\Employee;

class SalaryCalculatorContext
{
    /**
     * @var SalaryCalculatorStrategyInterface[]
     */
    private $strategies;

    /**
     * SalaryCalculatorContextI constructor.
     * @param SalaryCalculatorStrategyInterface[] $strategies
     */
    public function __construct(iterable $strategies)
    {
        $this->strategies = $strategies;
    }

    /**
     * @param Employee $employee
     * @param \DateTimeInterface $date
     * @return int
     * @throws NotSupportedStrategyException
     */
    public function calculate(Employee $employee, \DateTimeInterface $date): int
    {
        $salaryType = $employee->getSalaryType();

        foreach ($this->strategies as $strategy) {
            if ($strategy->supports($salaryType)) {
                return $strategy->calculateSalary($employee, $date);
            }
        }

        throw new NotSupportedStrategyException('Unsupported strategy: '.$salaryType);
    }
}
