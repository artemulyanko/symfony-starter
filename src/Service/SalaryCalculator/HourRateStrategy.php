<?php


namespace App\Service\SalaryCalculator;


use App\DataProvider\TrackedHoursDataProvider;
use App\Model\Employee;

class HourRateStrategy implements SalaryCalculatorStrategyInterface
{
    /**
     * @var TrackedHoursDataProvider
     */
    private $hoursProvider;

    public function __construct(TrackedHoursDataProvider $dataProvider)
    {
        $this->hoursProvider = $dataProvider;
    }

    public function supports(string $salaryType): bool
    {
        return $salaryType === SalaryType::HOURLY;
    }

    public function calculateSalary(Employee $employee, \DateTimeInterface $date): int
    {
        $stampedHours = $this->hoursProvider->getTrackedHoursByEmployeeIdAndMonth(
            $employee->getId(),
            $date
        );

        return $stampedHours * $employee->getHourlyRate();
    }
}
