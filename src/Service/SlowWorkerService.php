<?php


namespace App\Service;


use Psr\Log\LoggerInterface;

class SlowWorkerService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function doSomeJob(): void
    {
        // May be email sending, saving logs etc.
        $start = new \DateTime();
        $this->logger->info('WORK STARTED AT '.$start->format('y:m:d h:i:s'));

        sleep(10);

        $end = new \DateTime();
        $this->logger->info('WORK IS DONE AT '.$end->format('y:m:d h:i:s'));
    }
}
