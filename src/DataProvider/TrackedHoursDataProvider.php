<?php


namespace App\DataProvider;


class TrackedHoursDataProvider
{
    public function getTrackedHoursByEmployeeIdAndMonth(int $employeeId, \DateTimeInterface $date)
    {
        //mock
        $month = (int) $date->format('n');

        return $month * 5 * random_int(1 ,4) * $employeeId / 100;
    }
}
