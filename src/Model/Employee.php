<?php


namespace App\Model;


class Employee
{
    private $id;
    private $name;
    private $salaryType;
    private $hourlyRate;
    private $fixedSalary;

    public function __construct(
        string $name,
        string $salaryType,
        int $hourlyRate = null,
        int $fixedSalary = null
    ) {
        $this->id = random_int(1, 1000);
        $this->name = $name;
        $this->salaryType = $salaryType;
        $this->hourlyRate = $hourlyRate;
        $this->fixedSalary = $fixedSalary;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSalaryType(): string
    {
        return $this->salaryType;
    }

    public function setSalaryType(string $salaryType): void
    {
        $this->salaryType = $salaryType;
    }

    public function getHourlyRate(): int
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(int $hourlyRate): void
    {
        $this->hourlyRate = $hourlyRate;
    }

    public function getFixedSalary(): int
    {
        return $this->fixedSalary;
    }

    public function setFixedSalary(int $fixedSalary): void
    {
        $this->fixedSalary = $fixedSalary;
    }
}
