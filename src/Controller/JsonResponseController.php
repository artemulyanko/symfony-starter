<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/json/response")
 */
class JsonResponseController extends AbstractController
{
    public function __invoke()
    {
        return [
            'message' => 'Welcome to your new controller! If it works, you are done!',
            'path' => 'src/Controller/JsonResponseController.php',
        ];
    }
}
