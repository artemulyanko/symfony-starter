<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/with/html")
 */
class WithHtmlController extends AbstractController
{
    public function __invoke()
    {
        $response = new Response();

        $response->setContent('
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Document</title>
            </head>
            <body>
            <h1>Hello world!</h1>
            </body>
            </html>
        ');

        return $response;
    }
}
