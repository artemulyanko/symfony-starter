<?php

namespace App\Controller;

use App\Model\Employee;
use App\Service\SalaryCalculator\SalaryCalculatorContext;
use App\Service\SalaryCalculator\SalaryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;

/**
 * @Route("/")
 */
class StrategyController extends AbstractController
{
    /**
     * @throws \App\Service\SalaryCalculator\NotSupportedStrategyException
     */
    public function __invoke(SalaryCalculatorContext $calculatorContext)
    {
        $hourlyWorkingEmployee = new Employee(
            'Dummy Hourly',
            SalaryType::HOURLY
        );
        $hourlyWorkingEmployee->setHourlyRate(10);

        $fixedSalaryEmployee = new Employee(
            'Dummy Fixed',
            SalaryType::MONTHLY
        );
        $fixedSalaryEmployee->setFixedSalary(1000);

        return $this->json([
            [
                'employeeName' => $hourlyWorkingEmployee->getName(),
                'salary' => $calculatorContext->calculate($hourlyWorkingEmployee, new \DateTime())
            ],
            [
                'employeeName' => $fixedSalaryEmployee->getName(),
                'salary' => $calculatorContext->calculate($fixedSalaryEmployee, new \DateTime())
            ]
        ]);
    }
}
