<?php

namespace App\Controller;

use App\Service\SlowWorkerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/slow/worker")
 */
class SlowWorkerController extends AbstractController
{
    public function __invoke(SlowWorkerService $service): JsonResponse
    {
        $service->doSomeJob();

        return $this->json([
            'message' => 'Welcome to your new SLOW controller!',
            'path' => 'src/Controller/SlowWorkerController.php',
        ]);
    }
}
