<?php

namespace App\Controller;

use App\Exception\VeryCustomException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/with/exception")
 */
class WithExceptionController extends AbstractController
{
    /**
     * @throws VeryCustomException
     */
    public function __invoke()
    {
        throw new VeryCustomException('Some Error Happened!');
    }
}
